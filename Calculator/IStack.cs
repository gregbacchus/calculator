﻿namespace Calculator
{
    public interface IStack
    {
        void Push( object item );
        void Clear();
        bool TryPop( out object value );
        bool TryPop<T>( out T value );
        bool TryPeek( out object value );
    }
}