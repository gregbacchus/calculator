﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;

namespace Calculator
{
    public class ObservableStack : ObservableCollection<object>, IStack
    {
        public void Push( object item )
        {
            lock( this )
                Add( item );
        }

        public bool TryPop( out object value )
        {
            lock( this )
            {
                if( Count == 0 )
                {
                    value = null;
                    return false;
                }
                value = this[Count - 1];
                RemoveAt( Count - 1 );
                return true;
            }
        }

        public bool TryPop<T>( out T value )
        {
            lock( this )
            {
                if( Count == 0 )
                {
                    value = default( T );
                    return false;
                }

                var raw = this[Count - 1];
                if( raw is T )
                {
                    value = (T)raw;
                    RemoveAt( Count - 1 );
                    return true;
                }

                try
                {
                    var converted = Convert.ChangeType( raw, typeof( T ), CultureInfo.CurrentCulture );
                    if( converted != (object)default( T ) )
                    {
                        value = (T)converted;
                        RemoveAt( Count - 1 );
                        return true;
                    }
                }
                catch( InvalidCastException )
                {
                }

                value = default( T );
                return false;
            }
        }

        public bool TryPeek( out object value )
        {
            lock( this )
            {
                if( Count == 0 )
                {
                    value = null;
                    return false;
                }
                value = this[Count - 1];
                return true;
            }
        }
    }
}