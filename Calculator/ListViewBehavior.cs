﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;

namespace Calculator
{
    public class ListViewBehavior
    {
        private static readonly Dictionary<ListView, Capture> Associations = new Dictionary<ListView, Capture>();

        public static bool GetScrollOnNewItem( DependencyObject obj )
        {
            return (bool)obj.GetValue( ScrollOnNewItemProperty );
        }

        public static void SetScrollOnNewItem( DependencyObject obj, bool value )
        {
            obj.SetValue( ScrollOnNewItemProperty, value );
        }

        public static readonly DependencyProperty ScrollOnNewItemProperty =
            DependencyProperty.RegisterAttached(
                "ScrollOnNewItem",
                typeof( bool ),
                typeof( ListViewBehavior ),
                new UIPropertyMetadata( false, OnScrollOnNewItemChanged ) );

        public static void OnScrollOnNewItemChanged( DependencyObject d, DependencyPropertyChangedEventArgs e )
        {
            var listBox = d as ListView;
            if( listBox == null ) return;

            bool oldValue = (bool)e.OldValue, newValue = (bool)e.NewValue;
            if( newValue == oldValue ) return;

            if( newValue )
            {
                listBox.Loaded += ListBoxLoaded;
                listBox.Unloaded += ListBoxUnloaded;
            }
            else
            {
                listBox.Loaded -= ListBoxLoaded;
                listBox.Unloaded -= ListBoxUnloaded;
                if( Associations.ContainsKey( listBox ) )
                    Associations[listBox].Dispose();
            }
        }

        static void ListBoxUnloaded( object sender, RoutedEventArgs e )
        {
            var listBox = (ListView)sender;
            if( Associations.ContainsKey( listBox ) )
                Associations[listBox].Dispose();
            listBox.Unloaded -= ListBoxUnloaded;
        }

        static void ListBoxLoaded( object sender, RoutedEventArgs e )
        {
            var listBox = (ListView)sender;
            var incc = listBox.Items as INotifyCollectionChanged;
            if( incc == null ) return;
            listBox.Loaded -= ListBoxLoaded;
            Associations[listBox] = new Capture( listBox );
        }

        class Capture : IDisposable
        {
            private readonly ListView _listBox;
            private readonly INotifyCollectionChanged _incc;

            public Capture( ListView listBox )
            {
                _listBox = listBox;
                _incc = listBox.ItemsSource as INotifyCollectionChanged;
                if( _incc != null )
                {
                    _incc.CollectionChanged += OnCollectionChanged;
                }
            }

            void OnCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
            {
                if( e.Action != NotifyCollectionChangedAction.Add ) return;

                _listBox.ScrollIntoView( e.NewItems[0] );
                //_listBox.SelectedItem = e.NewItems[0];
            }

            public void Dispose()
            {
                if( _incc != null )
                    _incc.CollectionChanged -= OnCollectionChanged;
            }
        }
    }
}