﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Calculator
{
    public class EmptyToVisibilityConverter : MarkupExtension, IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if( !( value is string ) ) return Visibility.Collapsed;

            var s = (string)value;
            return string.IsNullOrWhiteSpace( s ) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotSupportedException();
        }

        public override object ProvideValue( IServiceProvider serviceProvider )
        {
            return this;
        }
    }
}