﻿using System.ComponentModel.Composition;

namespace Calculator
{
    [InheritedExport]
    public interface IOperation
    {
        string[] Identifiers { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stack">Stack to operate on</param>
        /// <param name="message">output message if there is an error</param>
        /// <returns>true: processed; false: error; null: not processed</returns>
        bool? Execute( IStack stack, out string message );
    }
}