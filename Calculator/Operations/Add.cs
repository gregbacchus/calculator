﻿namespace Calculator.Operations
{
    public class Add : IOperation
    {
        public string[] Identifiers
        {
            get { return new[] { "add", "+" }; }
        }

        public bool? Execute( IStack stack, out string message )
        {
            message = null;
            decimal? raw;

            if( !stack.TryPop( out raw ) || !raw.HasValue )
            {
                message = "could not parse stack item 'a'";
                return false;
            }
            var a = raw.Value;

            if( !stack.TryPop( out raw ) || !raw.HasValue )
            {
                message = "could not parse stack item 'b'";
                return false;
            }
            var b = raw.Value;

            stack.Push( a + b );
            return true;
        }
    }
}