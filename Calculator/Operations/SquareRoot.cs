﻿using System;

namespace Calculator.Operations
{
    public class SquareRoot : IOperation
    {
        public string[] Identifiers
        {
            get { return new[] { "sqrt", "2/" }; }
        }

        public bool? Execute( IStack stack, out string message )
        {
            message = null;
            decimal? raw;

            if( !stack.TryPop( out raw ) || !raw.HasValue )
            {
                message = "could not parse stack item 'a'";
                return false;
            }
            var a = raw.Value;

            stack.Push( (decimal)Math.Sqrt( (double)a ) );
            return true;
        }
    }
}