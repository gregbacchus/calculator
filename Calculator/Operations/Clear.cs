﻿namespace Calculator.Operations
{
    public class Clear : IOperation
    {
        public string[] Identifiers
        {
            get { return new[] { "del", "delete", "clear" }; }
        }

        public bool? Execute( IStack stack, out string message )
        {
            message = null;
            object o;
            stack.TryPop( out o );
            return true;
        }
    }
}