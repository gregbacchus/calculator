﻿namespace Calculator.Operations
{
    public class Reciprocal : IOperation
    {
        public string[] Identifiers
        {
            get { return new[] { "recip", "reciprocal" }; }
        }

        public bool? Execute( IStack stack, out string message )
        {
            message = null;
            decimal? raw;

            if( !stack.TryPop( out raw ) || !raw.HasValue )
            {
                message = "could not parse stack item 'a'";
                return false;
            }
            var a = raw.Value;

            stack.Push( 1 / a );
            return true;
        }
    }
}