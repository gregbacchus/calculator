﻿namespace Calculator.Operations
{
    public class Drop : IOperation
    {
        public string[] Identifiers
        {
            get { return new[] { "drop" }; }
        }

        public bool? Execute( IStack stack, out string message )
        {
            message = null;
            object o;
            stack.TryPop( out o );
            return true;
        }
    }
}