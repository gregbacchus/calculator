﻿namespace Calculator.Operations
{
    public class Swap : IOperation
    {
        public string[] Identifiers
        {
            get { return new[] { "swp", "swap" }; }
        }

        public bool? Execute( IStack stack, out string message )
        {
            message = null;
            object a, b;

            if( !stack.TryPop( out a ) )
            {
                message = "could not parse stack item 'a'";
                return false;
            }

            if( !stack.TryPop( out b ) )
            {
                message = "could not parse stack item 'a'";
                return false;
            }

            stack.Push( a );
            stack.Push( b );
            return true;
        }
    }
}