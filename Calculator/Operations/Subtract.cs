﻿namespace Calculator.Operations
{
    public class Subtract : IOperation
    {
        public string[] Identifiers
        {
            get { return new[] { "sub", "-" }; }
        }

        public bool? Execute( IStack stack, out string message )
        {
            message = null;
            decimal? raw;

            if( !stack.TryPop( out raw ) || !raw.HasValue )
            {
                message = "could not parse stack item 'a'";
                return false;
            }
            var a = raw.Value;

            if( !stack.TryPop( out raw ) || !raw.HasValue )
            {
                message = "could not parse stack item 'b'";
                return false;
            }
            var b = raw.Value;

            stack.Push( b - a );
            return true;
        }
    }
}