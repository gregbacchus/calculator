﻿namespace Calculator.Operations
{
    public class Duplicate : IOperation
    {
        public string[] Identifiers
        {
            get { return new[] { "dup", "duplicate" }; }
        }

        public bool? Execute( IStack stack, out string message )
        {
            message = null;
            object raw;

            if( !stack.TryPeek( out raw ) )
            {
                message = "could not parse stack item";
                return false;
            }

            stack.Push( raw );
            return true;
        }
    }
}