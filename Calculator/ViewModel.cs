﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Calculator.Annotations;
using Calculator.Operations;

namespace Calculator
{
    public class ViewModel : INotifyPropertyChanged, IRpnEngine
    {
        public ViewModel()
        {
            var catalog = new AssemblyCatalog( typeof( ViewModel ).Assembly );
            var container = new CompositionContainer( catalog );
            _operations = container.GetExportedValues<IOperation>().ToArray();
        }

        #region CurrentLine

        private string _currentLine;

        public string CurrentLine
        {
            [DebuggerStepThrough]
            get { return _currentLine; }
            [DebuggerStepThrough]
            set
            {
                if( Equals( _currentLine, value ) ) return; // Guard change notification
                _currentLine = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region History

        private readonly ObservableStack _hisory = new ObservableStack();

        public ObservableStack History
        {
            [DebuggerStepThrough]
            get { return _hisory; }
        }

        #endregion

        #region Message

        private string _message;

        public virtual string Message
        {
            [DebuggerStepThrough]
            get { return _message; }
            [DebuggerStepThrough]
            set
            {
                if( Equals( _message, value ) ) return; // Guard change notification
                _message = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region EnterLine

        private DelegateCommand _enterLine;

        public ICommand EnterLine
        {
            get { return _enterLine ?? (_enterLine = new DelegateCommand( OnEnterLine )); }
        }

        private void OnEnterLine( object args )
        {
            var line = ( CurrentLine ?? string.Empty );
            if( string.IsNullOrWhiteSpace( line ) )
                line = "dup";
            CurrentLine = Process( line + "\n" );
        }

        #endregion

        #region SpecialKey

        private DelegateCommand _specialKey;

        public ICommand SpecialKey
        {
            get { return _specialKey ?? (_specialKey = new DelegateCommand( OnSpecialKey )); }
        }

        private void OnSpecialKey( object args )
        {
            var key = (Key)args;
            switch( key )
            {
                case Key.Enter:
                    var line = ( CurrentLine ?? string.Empty );
                    if( string.IsNullOrWhiteSpace( line ) )
                        line = "dup";
                    CurrentLine = Process( line + "\n" );
                    break;
                case Key.Divide:
                    CurrentLine = ProcessCurrentAnd( "/\n" );
                    break;
                case Key.Multiply:
                    CurrentLine = ProcessCurrentAnd( "*\n" );
                    break;
                case Key.Subtract:
                    CurrentLine = ProcessCurrentAnd( "-\n" );
                    break;
                case Key.Add:
                    CurrentLine = ProcessCurrentAnd( "+\n" );
                    break;
            }
        }

        #endregion

        private readonly IOperation[] _operations;

        public IOperation[] Operations { get { return _operations; } }

        public string ProcessCurrentAnd( string data )
        {
            return Process( (!string.IsNullOrWhiteSpace( CurrentLine ) ? CurrentLine + "\n" : string.Empty) + data );
        }

        public string Process( string data )
        {
            Message = null;
            var parts = data.Split( '\n' );

            if( parts.Length > 1 )
            {
                parts.Take( parts.Length - 1 )
                     .ToList()
                     .ForEach( Execute );
            }

            return parts[parts.Length - 1];
        }

        private void Execute( string line )
        {
            var operations = Operations.ToArray();
            try
            {
                foreach( var operation in operations.Where( o => o.Identifiers.Any( n => string.Equals( n, line, StringComparison.InvariantCultureIgnoreCase ) ) ) )
                {
                    string message;
                    var result = operation.Execute( History, out message );
                    if( !result.HasValue )
                        continue;
                    if( !result.Value )
                    {
                        Message = message;
                    }
                    return;
                }

                decimal number;
                if( decimal.TryParse( line, NumberStyles.Any, CultureInfo.CurrentCulture, out number ) )
                {
                    History.Push( number );
                    return;
                }

                TimeSpan time;
                if( TimeSpan.TryParse( line, CultureInfo.CurrentCulture, out time ) )
                {
                    History.Push( time );
                    return;
                }

                DateTime date;
                if( DateTime.TryParse( line, CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal, out date ) )
                {
                    History.Push( date );
                    return;
                }

                History.Push( line );
            }
            catch( Exception exception )
            {
                Trace.WriteLine( exception.ToString() );
                Message = exception.Message;
            }
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged( [CallerMemberName] string propertyName = null )
        {
            var handler = PropertyChanged;
            if( handler != null )
                handler( this, new PropertyChangedEventArgs( propertyName ) );
        }

        #endregion

    }
}