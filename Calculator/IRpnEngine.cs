﻿namespace Calculator
{
    public interface IRpnEngine
    {
        string Process( string data );
    }
}